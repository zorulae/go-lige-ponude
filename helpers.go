package main

import (
	"fmt"
	"ligeponude/service"
)

type Lige struct {
	Lige []Liga `json:"lige"`
}

type Liga struct {
	Naziv   string    `json:"naziv"`
	Razrade []Razrada `json:"razrade"`
}

type Razrada struct {
	Tipovi []Tip `json:"-"`
	Ponude []int `json:"ponude"`
}

type Tip struct {
	Naziv string `json:"naziv"`
}

type Ponuda struct {
	Broj          string  `json:"broj"`
	Tvkanal       string  `json:"tv_kanal,omitempty"`
	Id            int     `json:"id"`
	Naziv         string  `json:"naziv"`
	ImaStatistiku bool    `json:"ima_statistiku,omitempty"`
	Vrijeme       string  `json:"vrijeme"`
	Tecajevi      []Tecaj `json:"tecajevi"`
}

type Tecaj struct {
	Tecaj float32 `json:"tecaj"`
	Naziv string  `json:"naziv"`
}

func LoadLeagues(target *Lige) error {
	const url = "https://minus5-dev-test.s3.eu-central-1.amazonaws.com/lige.json"

	err := service.LoadJson(url, &target)
	if err != nil {
		fmt.Printf("error getting leagues: %s\n", err.Error())
	}

	return err
}

func LoadPonude(target *[]Ponuda) {
	const url = "https://minus5-dev-test.s3.eu-central-1.amazonaws.com/ponude.json"

	err := service.LoadJson(url, &target)
	if err != nil {
		fmt.Printf("error getting ponude: %s\n", err.Error())
	}
}
