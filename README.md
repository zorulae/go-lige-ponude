# go-lige-ponude

A simple CRUD app built using Go for displaying sample betting offers.

## Prerequisites
[Go](https://go.dev/doc/install)

## Installation

```bash
# clone the repository
git clone https://gitlab.com/zorulae/go-lige-ponude.git

# navigate into the project directory
cd go-lige-ponude

# install dependencies
go get

# build the binary
go build -o app

# start the service
./app
```

The application should now be running on http://localhost:8080.
