package main

import (
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

var lige Lige
var ponude []Ponuda

func getLige(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(lige)
}

func getPonude(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(ponude)
}

func getPonuda(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		respondWithError(w, 404)
	}

	for _, item := range ponude {
		if item.Id == id {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
}

func createPonuda(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var ponuda Ponuda
	_ = json.NewDecoder(r.Body).Decode(&ponuda)
	ponuda.Id = rand.Intn(100000000)
	ponude = append(ponude, ponuda)
	json.NewEncoder(w).Encode(ponuda)
}

func respondWithError(w http.ResponseWriter, code int) error {
	response, err := json.Marshal(map[string]string{"success": "false", "message": "Something went wrong"})
	if err != nil {
		return err
	}
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.WriteHeader(code)
	w.Write(response)
	return nil
}

func main() {
	r := mux.NewRouter()

	LoadLeagues(&lige)
	LoadPonude(&ponude)

	r.HandleFunc("/lige", getLige).Methods("GET")
	r.HandleFunc("/ponude", getPonude).Methods("GET")
	r.HandleFunc("/ponude/{id}", getPonuda).Methods("GET")
	r.HandleFunc("/ponude", createPonuda).Methods("POST")

	fmt.Printf("Starting server at port 8080\n")
	if err := http.ListenAndServe(":8080", r); err != nil {
		log.Fatal(err)
	}
}
