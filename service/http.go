package service

import (
	"encoding/json"
	"net/http"
	"time"
)

var client *http.Client

func LoadJson(url string, target interface{}) error {
	client = &http.Client{Timeout: 10 * time.Second}

	resp, err := client.Get(url)
	if err != nil {
		return err
	}

	defer resp.Body.Close()

	return json.NewDecoder(resp.Body).Decode(&target)
}
